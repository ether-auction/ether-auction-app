import { gql } from "apollo-boost";

// See more example queries on https://thegraph.com/explorer/subgraph/paulrberg/create-eth-app

const GET_AUCTION = gql`  query {
    myAuctions {
      id
      pot
      start_time
      end_time
      expiration_time 
  }
}`;

const GET_TRANSFERS = gql` 
    query {
        transfers(first:10) {
            id
            from
            to
            value
        }
     } 
`;


export default GET_AUCTION;
