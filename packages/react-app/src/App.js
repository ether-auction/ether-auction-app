import React from "react";

import { Contract } from "@ethersproject/contracts";
import { getDefaultProvider } from "@ethersproject/providers";
import { useQuery } from "@apollo/react-hooks";

import { Body, Button, Header, Image, Link } from "./components";
import logo from "./ethereumLogo.png";
import useWeb3Modal from "./hooks/useWeb3Modal";

import { addresses, abis } from "@project/contracts";
import GET_AUCTION from "./graphql/subgraph";

const AUCTION = {
  pot: 0,

};

async function readOnChainData() {
  const defaultProvider = getDefaultProvider("http://localhost:8545");
  //await window.ethereum.enable();
  const signer = defaultProvider.getSigner();
  console.log(signer);
  const auction = new Contract(addresses.auction, abis.auction, defaultProvider);

  const auctionPot = await auction.pot()
      .catch(err => {
    console.log(err.toString());
    defaultProvider.getBlockNumber().then(console.log)
  });
  AUCTION.pot = auctionPot;
  console.log({ auctionBalance: auctionPot.toString() });


}

function WalletButton({ provider, loadWeb3Modal, logoutOfWeb3Modal }) {
  return (
    <Button
      onClick={() => {
        if (!provider) {
          loadWeb3Modal();
        } else {
          logoutOfWeb3Modal();
        }
      }}
    >
      {!provider ? "Connect Wallet" : "Disconnect Wallet"}
    </Button>
  );
}

class Auction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pot: 0,
      start: 0,
      end: 0,
      expiration: 0,
      win_bid: 0,
      win_bidder: 0,
      lose_bid: 0,
      lose_bidder: 0,
    }
  }

  render() {
    return (
        <div>
          <h1>Auction Details</h1>
          <ul>
            <li>Pot: {this.state.pot}</li>
          </ul>
        </div>
    );
  }
}

function App() {
  // const { loading, error, data } = useQuery(GET_TRANSFERS);
  const { loading, error, data } = useQuery(GET_AUCTION);
  const [provider, loadWeb3Modal, logoutOfWeb3Modal] = useWeb3Modal();

  React.useEffect(() => {
      // if (!loading && !error && data && data.transfers) {
      // console.log({ transfers: data.transfers });

      if (!loading && !error && data && data.auctions) {
      console.log({ auctions: data.auctions });
    }
  }, [loading, error, data]);

  return (
    <div>
      <Header>
        <WalletButton provider={provider} loadWeb3Modal={loadWeb3Modal} logoutOfWeb3Modal={logoutOfWeb3Modal} />
      </Header>
      <Body>
        <Image src={logo} alt="react-logo" />
        <p>
          Ether Auction
        </p>

        {/*<Button onClick={() => readOnChainData()}>*/}
        {/*  Get Auction Details*/}
        {/*</Button>*/}
        <Auction/>
      </Body>
    </div>
  );
}

export default App;
