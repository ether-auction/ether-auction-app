import { BigInt } from "@graphprotocol/graph-ts"
import {
  Auction,
  BidEvent,
  CanceledEvent,
  ClaimedEvent,
  CompletedEvent,
  StartEvent,
  WithdrawalEvent
} from "../generated/Auction/Auction"
import { Bid, MyAuction } from "../generated/schema"

export function handleBidEvent(event: BidEvent): void {
  let id = event.transaction.hash.toHex()
  // Entities can be loaded from the store using a string ID; this ID
  // needs to be unique across all entities of the same type
  let entity = Bid.load(id)

  // Entities only exist after they have been saved to the store;
  // `null` checks allow to create entities on demand
  if (entity == null) {
    entity = new Bid(id)

    // Entity fields can be set using simple assignments
    entity.count = BigInt.fromI32(0)
  }

  // BigInt and BigDecimal math are supported
  entity.count = entity.count + BigInt.fromI32(1)

  // Entity fields can be set based on event parameters
  entity.highest_bidder = event.params.highest_bidder
  entity.highest_bid = event.params.highest_bid

  // Entities can be written to the store with `.save()`
  entity.save()

  // Note: If a handler doesn't require existing field values, it is faster
  // _not_ to load the entity from the store. Instead, create it fresh with
  // `new Entity(...)`, set the fields that should be updated and save the
  // entity back to the store. Fields that were not set or unset remain
  // unchanged, allowing for partial updates to be applied.

  // It is also possible to access smart contracts from mappings. For
  // example, the contract that has emitted the event can be connected to
  // with:
  //
  // let contract = Contract.bind(event.address)
  //
  // The following functions can then be called on this contract to access
  // state variables and other data:
  //
  // - contract.STATE(...)
  // - contract.auction_end(...)
  // - contract.auction_expiration(...)
  // - contract.auction_start(...)
  // - contract.bid_time(...)
  // - contract.bids(...)
  // - contract.cancel_auction(...)
  // - contract.claim_earnings(...)
  // - contract.claim_pot(...)
  // - contract.complete(...)
  // - contract.expiration_time(...)
  // - contract.highest_bid(...)
  // - contract.highest_bidder(...)
  // - contract.is_active(...)
  // - contract.is_completed(...)
  // - contract.loser_bid(...)
  // - contract.loser_bidder(...)
  // - contract.pot(...)
  // - contract.withdraw(...)
}

export function handleCanceledEvent(event: CanceledEvent): void {}

export function handleClaimedEvent(event: ClaimedEvent): void {}

export function handleCompletedEvent(event: CompletedEvent): void {}

export function handleStartEvent(event: StartEvent): void {
  let auction = Auction.bind(event.address)
  let id = event.transaction.hash.toHex()
  let entity = MyAuction.load(id)

  if (entity == null) {
    entity = new MyAuction(id)
    entity.pot = auction.pot()
    entity.start_time = auction.auction_start()
    entity.end_time = auction.auction_end()
    entity.expiration_time = auction.auction_expiration()
  }

  entity.save()
}

export function handleWithdrawalEvent(event: WithdrawalEvent): void {}
