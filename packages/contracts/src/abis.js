import auctionAbi from "./abis/MyAuction";

const abis = {
  auction: auctionAbi,
};

export default abis;
